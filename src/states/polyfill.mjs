/**
 * Utils for dealing with AO3 interactive fiction generated HTML
 * Copyright (C) 2024  ocremaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {Context as WarningContext, Warning} from '../warnings.mjs'

/**
 * Custom class meant to handle states.
 * Provides a warning pool.
 */
export class StatesPolyfill extends WarningContext {
    /**
     *
     * @param {HTMLElement} work
     * @param {Location} loc
     */
    constructor(work, loc = location) {
        super()
        this.work = work
        this.location = loc
    }

    /**
     * Resets the states data.
     */
    apply() {
        super.reset()
        this.allowedMaximum = 0
        this.activeStates = new Set()
        this.traverseTree()
    }

    /**
     * Parses the value of a states attribute.
     * @param {string} value
     * @returns {number[]}
     */
    parseStatesAttribute(value) {
        return value.split(',').map(s => parseInt(s))
    }

    /**
     * Removes states from hash link
     * @param {string} hash
     * @returns {string}
     */
    removeStatesFromHash(hash) {
        if(hash.includes('+s'))
            return hash.split('+s')[0] + '+'
        else
            return hash
    }

    /**
     * Activates and deactivates a bunch of states, and returns a link corresponding to the given canonicalLink
     * (or the current canonical hash if none is provided).
     * @param {int[]} activate
     * @param {int[]} deactivate
     * @param {string|null} canonicalLink
     */
    applyStatesAndReturnLink(activate, deactivate, canonicalLink = null) {
        if(canonicalLink == null)
            throw new Error("No canonical link provided.")
        for(let state of activate)
            this.activeStates.add(state)
        for(let state of deactivate)
            this.activeStates.delete(state)
        // Sort states
        this.activeStates = new Set(Array.from(this.activeStates).sort((a, b) => a - b))
        // Build links
        let link = canonicalLink
        for(let state of this.activeStates)
            link += `s${state}+`
        // Check if named anchor exists
        let paragraph = document.querySelector("#workskin > p[class*=theme]")
        let anchor = paragraph.querySelector(`a[id="${link}"]`)
        if(anchor == null && link !== "") {
            anchor = document.createElement("a")
            anchor.id = link
            anchor.name = link
            paragraph.insertBefore(anchor, paragraph.firstChild)
        }
        return "#" + link
    }


    /**
     * Called when a link is clicked.
     * @param {Event} e
     * @param {HTMLAnchorElement} link
     */
    _linkClicked(e, link) {
        let hrefStates = this.applyStatesAndReturnLink(
            this.parseStatesAttribute(link.getAttribute("activate-state") || ''),
            this.parseStatesAttribute(link.getAttribute("deactivate-state") || ''),
            this.removeStatesFromHash(link.href.split('#')[1])
        )
        console.log("Activating", hrefStates)
        let href = link.href
        if(hrefStates !== link.href)
            link.setAttribute("href", hrefStates)
        setTimeout(() => link.setAttribute("href", href), 100)
    }

    /**
     * Traverses the HTML tree from work, checks for "BEGIN STATES:" comments
     * and adds a "max-states" attributes to each link after said comments.
     * Also checks if the activate/deactivate are within the maximum allowed.
     */
    traverseTree(parent = this.work) {
        for(let node of parent.childNodes) {
            if(node.nodeType === Node.COMMENT_NODE) {
                let contents = node.textContent.trim()
                if(contents.startsWith("BEGIN STATES: "))
                    this.allowedMaximum = parseInt(contents.split(" ").pop())
            } else if(node.nodeType === Node.ELEMENT_NODE) {
                if(node.nodeName === "A" && node.hasAttribute("href")) {
                    let activate = []
                    let deactivate = []
                    if(node.hasAttribute("activate-state"))
                        activate = this.parseStatesAttribute(node.getAttribute("activate-state"))
                    if(node.hasAttribute("deactivate-state"))
                        deactivate = this.parseStatesAttribute(node.getAttribute("deactivate-state"))
                    let changed = [...activate, ...deactivate]
                    let invalid = changed.filter(s => s > this.allowedMaximum)
                    if(invalid.length > 0) {
                        let msg = `${node.outerHTML} is trying to set state(s) ${invalid.join(", ")} despite the maximum being ${this.allowedMaximum}.
                                          State(s) ${invalid.join(", ")} WILL be reset to inactive after pressing this button.`
                        this.warn(new Warning(Warning.WARN, "Invalid state set", msg))
                    }
                    let conflicts = activate.filter(s => deactivate.includes(s))
                    if(conflicts.length > 0) {
                        let msg = `${node.outerHTML} is trying to both activate and deactivate state(s) ${conflicts.join(", ")}. 
                                          States will be deactivated.`
                        this.warn(new Warning(Warning.WARN, "Conflictual state set", msg))
                    }
                    node.setAttribute("max-states", this.allowedMaximum)
                    node.addEventListener('click', (e) => {
                        this._linkClicked(e, node)
                    })
                }
                this.traverseTree(node)
            }
        }
    }
}