/**
 * Utils for dealing with AO3 interactive fiction generated HTML
 * Copyright (C) 2024  ocremaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export class Warning extends Error {
    static INFO = Symbol("INFO")
    static WARN = Symbol("WARNING")
    static CRITICAL = Symbol("CRITICAL")
    static ERROR = Symbol("ERROR")

    constructor(type, title, message) {
        super(message);
        this.type = type
        this.title = title;
        this.message = message;
    }

    static Pool
}

/**
 * Context for handling warnings
 */
export class Context {
    constructor() {
        this._warnings = []
    }

    warn(warning) {
        this._warnings.push(warning)
    }

    warnings() {
        return this._warnings
    }

    reset() {
        this._warnings = []
    }
}